### PyTorch Example 

### What is PyTorch?
PyTorch is a fully deatured framework for building deep learning models. This is a type of machine learning that is commonly used for models that will be used for image- Object Detection. PyTorch serves as a programming tool with its vast range of libraryies that help programmers (such as you!) to build, train, and test complex models with extreme efficiency.

### Downloading PyTorch
Once you log on to a development node on the HPCC, run the following command to download the PyTorch environment"

pip3 install torch==1.8.1+cu102 torchvision==0.9.1+cu102 torchaudio===0.8.1 -f https://download.pytorch.org/whl/torch_stable.html

### Running the example
This example is extremely basic.
It shows how you can run code on multiple GPU's (parallelize) code to make it more efficient and run much faster. Lets explore what to do

In this repository there is a file named: pytorch_parallel_example.py

lets see how to run it:

# Step 1:
Open a development node on the HPCC

# Step 2: 
Load the file pytorch_parallel_example.py on the HPCC

# Step 3: 
Running the file
- To run the file we first need to ensure that PyTorch is first downloaded
- If you did not follow the steps above in the 'Downloading PyTorch' section, please do so now.
- You can directly copy and paste the command in that section to download PyTorch

Once you have successfully downloaded PyTorch we can run the file
- using the following command you can run the example file:
    - python pytorch_parallel_example.py


## Source for example:
https://pytorch.org/tutorials/beginner/blitz/data_parallel_tutorial.html
