# PyTorch Object Detection Samandu1



## READ THIS BEFORE PROCEEDING

This repository is for Rohit Samandur's Final Project for CMSE401 at Michigan State University
The project aimed to explore Python's PyTorch package for use in object detection in an image. 
The project will utilize the MNIST dataset and show two different trained models. The first model
will be trained using unparallelizd code and the second model will be trained using parallelized code.

The goal of the project will be to show that parallelizing the training of these models will reduce runtime 
thus optimizing code and creating more efficient results to handle massive data sets

## Getting Started

To get started lets explore what file we are dealing with.

All code for this project is within the ipynb file CMSE401 Final Project.ipynb

Step1. 
    Open up the HPCC and navigate to a development node.

Step2. 
    Clone this repository using the following command:
        git clone https://gitlab.msu.edu/samandu1/pytorch-object-detection-samandu1.git

Step3. 
    Once cloned, open up Jupyter notebook on the HPCC and set your environment to have 2-4 cores

Step4. 
    Navigate to the cloned repository and open the file CMSE401 Final Prject.ipynb

Step5. 
    Once opened, follow the steps from the IPYNB to work through the project


## Footnotes
Project Created By: Rohit Samandur for CMSE401 Parallel Processing at Michigan State University SS23
Contact: samandu1@msu.edu
